# em-fceux-example-minimal

https://bitbucket.org/tsone/em-fceux-example-minimal/

Minimal [em-fceux](https://github.com/tsone/fceux/tree/emscripten/src/drivers/em) example.

## Build

1. Have [npm](https://www.npmjs.com/get-npm).
2. Run `npm install`.
3. Run `npm start` to serve at http://localhost:8080/.

## Contact

Authored by Valtteri "tsone" Heikkilä. See git commits for email.

Please submit bugs and feature requests at
[em-fceux issue tracker](https://bitbucket.org/tsone/em-fceux/issues?status=new&status=open).
