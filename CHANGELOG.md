# Changelog for em-fceux-example-minimal

## [1.0.1] - 2022-06-01

### Changed

- Migrated to em-fceux 2.1.0 (FCEUX fork on github).

## [1.0.0] - 2020-10-04

### Added

- Initial commit.
